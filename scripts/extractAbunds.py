import h5py 
import numpy as np 

def GetAbundVsA(fname): 
  """
  Calculate the isobaric abundance as a function of A
  """
  f = h5py.File(fname, 'r') 
  
  A = np.arange(0, np.max(f['A'])+1) 
  Ya = np.zeros(A.shape) 
  
  Ye0 = f['Ye'][0] 
   
  for (aa, yy) in zip(f['A'], f['Y'][-1][:]): 
    Ya[aa] += yy
  
  return (A, Ya, Ye0)     

def GetAbundVsZ(fname): 
  """
  Calculate the elemental abundance as a function of Z 
  """
  f = h5py.File(fname, 'r') 
  
  Z = np.arange(0, np.max(f['Z'])+1) 
  Yz = np.zeros(Z.shape) 
  
  Ye0 = f['Ye'][0] 
  
  for (zz, yy) in zip(f['Z'], f['Y'][-1][:]): 
    Yz[zz] += yy
  
  return (Z, Yz, Ye0)     

