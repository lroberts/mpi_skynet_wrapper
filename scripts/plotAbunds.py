import numpy as np 
import glob 
import matplotlib.pyplot as plt 

from extractAbunds import GetAbundVsA, GetAbundVsZ 


runs = sorted(glob.glob('*.h5'))

colormap = plt.get_cmap('viridis') 

plt.xlabel('A') 
plt.ylabel('Y(A)') 

for run in runs[-1:0:-1]: 
  print (run)
  (A, Ya, Ye) = GetAbundVsA(run)
  clr = colormap(Ye/0.5) 
  plt.semilogy(A, Ya, color=clr, linewidth=0.5) 


xmin = 0
xmax = 230 
ymin = 1.e-12
ymax = 0.01
plt.axis([xmin, xmax, ymin, ymax]) 

plt.savefig('Abundances.pdf', bbox_inches='tight')
