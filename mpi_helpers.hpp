#ifndef MPIHELPERS
#define MPIHELPERS

#include <string> 
#include <mpi.h> 

int MPI_Send_string(int dest, int tag, MPI_Comm comm, std::string str) {
   return MPI_Send(str.c_str(), str.length() + 1, MPI_CHAR, dest, tag, comm);
}

std::string MPI_Recv_string(int source, int tag, MPI_Comm comm) {
  int ndata; 
  MPI_Status status;
  MPI_Probe(source, tag, comm, &status); 
  MPI_Get_count(&status, MPI_CHAR, &ndata); 

  char data[ndata];
  MPI_Recv(data, ndata, MPI_CHAR, source, tag, comm, &status); 
  return std::string(data); 
}

#endif // MPIHELPERS