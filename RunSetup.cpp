/// \file base_run.cpp
/// \author lroberts
/// \since Jun 7, 2018
///
/// \brief
///
///
#include <string>
#include <vector> 
#include <algorithm> 
#include <map> 
#include <sstream> 

#include "RunSetup.hpp" 
#include "BuildInfo.hpp"
#include "EquationsOfState/HelmholtzEOS.hpp"
#include "EquationsOfState/SkyNetScreening.hpp"
#include "Network/ReactionNetwork.hpp"
#include "Network/NSE.hpp"
#include "DensityProfiles/ExpTMinus3.hpp"
#include "DensityProfiles/PowerLawContinuation.hpp"
#include "Reactions/REACLIBReactionLibrary.hpp"
#include "Reactions/FFNReactionLibrary.hpp"
#include "NuclearData/Nuclide.hpp"
#include "Utilities/Interpolators/PiecewiseLinearFunction.hpp"
#include "Utilities/Constants.hpp"

RunSetup::~RunSetup() { 
  for (auto& lib : reactionLibs) delete lib;  
} 
 
RunSetup::RunSetup(std::string runName, NetworkOptions optsIn) : 
      runName(runName),
      nuclib(NuclideLibrary::CreateFromWebnucleoXML(SkyNetRoot + "/data/webnucleo_nuc_v2.0.xml")),
      eos(HelmholtzEOS(SkyNetRoot + "/data/helm_table.dat"))
{ 
    opts = optsIn; 
    //opts.ConvergenceCriterion = NetworkConvergenceCriterion::Mass;
    //opts.MassDeviationThreshold = 1.0E-10;
    opts.IsSelfHeating = true;
    //opts.EnableScreening = doScreen;
    //opts.NSEEvolutionMinT9 = 7.0;

    LoadREACLIBLibraries();
} 

void RunSetup::LoadREACLIBLibraries() { 
    
  const ReactionLibraryBase* strongReacLib
      = new REACLIBReactionLibrary(SkyNetRoot + "/data/reaclib",
        ReactionType::Strong, true, LeptonMode::TreatAllAsDecayExceptLabelEC,
        "Strong reactions", nuclib, opts, true);
  reactionLibs.push_back(strongReacLib); 

  const ReactionLibraryBase* symFis
      = new REACLIBReactionLibrary(SkyNetRoot + 
        "/data/netsu_panov_symmetric_0neut", ReactionType::Strong, false,
        LeptonMode::TreatAllAsDecayExceptLabelEC,
        "Symmetric neutron induced fission with 0 free neutrons", nuclib, opts,
        false);
  reactionLibs.push_back(symFis); 

  const ReactionLibraryBase* spontFis 
      = new REACLIBReactionLibrary(SkyNetRoot + 
        "/data/netsu_sfis_Roberts2010rates", ReactionType::Strong, false,
        LeptonMode::TreatAllAsDecayExceptLabelEC, "Spontaneous fission", nuclib,
        opts, false);
  reactionLibs.push_back(spontFis); 

  
  // use only REACLIB weak rates
  const ReactionLibraryBase* weakLib
      = new REACLIBReactionLibrary(SkyNetRoot + "/data/reaclib",
      ReactionType::Weak, false, LeptonMode::TreatAllAsDecayExceptLabelEC,
      "Weak reactions", nuclib, opts, true);
  reactionLibs.push_back(weakLib); 


  // or use the following code to use FFN rates and weak REACLIB rates
  // pre-computed with the <SkyNetRoot>/examples/precompute_reaction_libs.py
  // script
//  auto ffnMesaReactionLibrary = FFNReactionLibrary::ReadFromDisk(
//      "ffnMesa_with_neutrino", opts);
//  auto ffnReactionLibrary = FFNReactionLibrary::ReadFromDisk(
//      "ffn_with_neutrino_ffnMesa", opts);
//  auto weakReactionLibrary = REACLIBReactionLibrary::ReadFromDisk(
//      "weak_REACLIB_with_neutrino_ffnMesa_ffn", opts);

  // add neutrino reactions, if desired (will need a neutrino distribution
  // function set with net.LoadNeutrinoHistory(...))
//  NeutrinoReactionLibrary neutrinoLibrary(SkyNetRoot
//     + "/data/neutrino_reactions.dat", "Neutrino interactions", nuclib, opts,
//     1.e-2, false, true);

}

int RunSetup::RunTauEvolution(double Ye, double s, double tau, double T0, double tend) {
    

  double tstart = 0.0; 

  SkyNetScreening screen(nuclib);
  ReactionNetwork net(nuclib, reactionLibs, &eos, &screen, opts);
  NSEOptions nseopt;
  nseopt.DoScreening = opts.EnableScreening;
  NSE nse(net.GetNuclideLibrary(), &eos, &screen, nseopt);
  
  // run NSE with the temperature and entropy to find the initial density
  auto nseResult = nse.CalcFromTemperatureAndEntropy(T0, s, Ye);
 
  auto densityProfile = ExpTMinus3(nseResult.Rho(), tau / 1000.0);
 
  auto output = net.EvolveSelfHeatingWithInitialTemperature(nseResult.Y(), tstart,
     tend, T0, &densityProfile, runName);
  
  // Write the final abundances to file  
  //std::string buffer = runName + ".yva";
  //FILE * f = fopen(buffer.c_str(), "w");
  //std::vector<double> finalYVsA = output.FinalYVsA();
  //for (unsigned int A = 0; A < finalYVsA.size(); ++A)
  //    fprintf(f, "%6i  %30.20E\n", A, finalYVsA[A]);
  //fclose(f);

  return 0; 
}


int RunSetup::RunFromFile(std::string fname, double T0, double tend) {
  std::vector<double> time, rho, T9, ye; 
  ReadProfiles(fname, &time, &rho, &T9, &ye);  
  
  PiecewiseLinearFunction rhoVsTime(time, rho, true); 
  PiecewiseLinearFunction T9VsTime(time, T9, true); 
  PiecewiseLinearFunction yeVsTime(time, ye, true); 
  double tstart = time[0]; 
  for (int i=time.size()-1; i>=0; --i) {
    if (T9VsTime(time[i])>T0) {
      tstart = time[i]; 
      break;
    }
  }
  
  SkyNetScreening screen(nuclib);
  ReactionNetwork net(nuclib, reactionLibs, &eos, &screen, opts);
  NSEOptions nseopt;
  nseopt.DoScreening = opts.EnableScreening;
  NSE nse(net.GetNuclideLibrary(), &eos, &screen, nseopt);
 
  auto nseResult = nse.CalcFromTemperatureAndDensity(T9VsTime(tstart), 
      rhoVsTime(tstart), yeVsTime(tstart));
  
  PowerLawContinuation densityProfile(rhoVsTime, -3.0, 1.e-3); 
  auto output = net.EvolveSelfHeatingWithInitialTemperature(nseResult.Y(), tstart,
     tend, T9VsTime(tstart), &densityProfile, runName);
  return 0; 
}

void RunSetup::ReadProfiles(std::string fname, std::vector<double>* time, 
    std::vector<double>* rho, std::vector<double>* T9, std::vector<double>* ye) {
  
  *time = std::vector<double>();
  *rho = std::vector<double>();
  *T9 = std::vector<double>();
  *ye = std::vector<double>();

  std::ifstream f(fname); 
  std::string line; 
  while (std::getline(f, line)) {
    
    // Remove portion of line that is commented out 
    size_t cidx = line.find("#");
    if (cidx != std::string::npos) line = line.substr(0, cidx); 

    // Remove whitespace from the beginning of the string
    if (line.size() > 0) line = line.substr(line.find_first_not_of(" \t"), line.size());
    
    if (line.size() > 0) {
      // Read in uncommented data
      double t_ms, rho_fm3, TMeV, sc, yec;    
      std::stringstream ss(line); 
      
      ss >> t_ms >> rho_fm3>> TMeV >> sc >> yec; 
      (*time).push_back(t_ms * 1.e-3); 
      (*rho).push_back(rho_fm3 * 1.e39 / Constants::AvogadroConstantInPerGram); 
      (*T9).push_back(TMeV / Constants::BoltzmannConstantInMeVPerGK); 
      (*ye).push_back(yec);
    }   
  } 


}



