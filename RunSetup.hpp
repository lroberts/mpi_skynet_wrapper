#ifndef RUNSETUP
#define RUNSETUP 

#include "BuildInfo.hpp"
#include "EquationsOfState/HelmholtzEOS.hpp"
#include "EquationsOfState/SkyNetScreening.hpp"
#include "Network/ReactionNetwork.hpp"
#include "Network/NSE.hpp"
#include "DensityProfiles/ExpTMinus3.hpp"
#include "Reactions/REACLIBReactionLibrary.hpp"
#include "NuclearData/Nuclide.hpp"
#include "Utilities/Interpolators/PiecewiseLinearFunction.hpp"


class RunSetup {
public: 
  RunSetup(std::string runName = "defaultRunName", NetworkOptions optsIn = NetworkOptions());
  ~RunSetup();
  
  NetworkOptions opts;
  std::string runName;
  
  int RunTauEvolution(double Ye, double s, double tau, double T0 = 6.0, double tend = 1.e9);

  int RunFromFile(std::string fname, double T0 = 6.0, double tend = 1.e9); 

private:
  NuclideLibrary nuclib;
  HelmholtzEOS eos;
  
  std::vector<const ReactionLibraryBase*> reactionLibs; 
  void LoadREACLIBLibraries(); 
  void ReadProfiles(std::string fname, PiecewiseLinearFunction* rhoVsTime, 
    PiecewiseLinearFunction* T9VsTime, PiecewiseLinearFunction* yeVsTime);
  void ReadProfiles(std::string fname, std::vector<double>* time, 
    std::vector<double>* rho, std::vector<double>* T9, std::vector<double>* ye);
};

#endif 
