/// MPI wrapper testing 
/// 
///


#include <string>
#include <vector> 
#include <algorithm> 
#include <map> 
#include <iostream>
#include <mpi.h> 
#include <boost/filesystem.hpp>
#include <boost/regex.hpp> 

#include "mpi_helpers.hpp"
#include "RunSetup.hpp" 

int main(int nargin, char** args) {
  
  // Perform standard MPI initialization 
  int world_rank, world_size;
  MPI_Init(NULL, NULL); 
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank); 
  MPI_Comm_size(MPI_COMM_WORLD, &world_size); 
   
  // Message tags  
  const int request_message_tag = 1;
  const int num_message_tag = 2;
  const int data_message_tag = 4;
   
  std::string baseDir =  "./";
  if (nargin>1) baseDir = std::string(args[1]); 
  
  std::string finishedDir =  "./";
  if (nargin>2) finishedDir = std::string(args[2]); 
  
  if (world_rank == 0) { 
    
    // Find all of the particle files we might want to run  
    const std::string target_path( baseDir );
    const boost::regex my_filter( "particle.*\.txt" );
    std::vector< std::string > all_matching_files;
    
    boost::filesystem::directory_iterator end_itr; // Default ctor yields past-the-end
    for( boost::filesystem::directory_iterator i( target_path ); i != end_itr; ++i )
    {
      // Skip if not a file
      if( !boost::filesystem::is_regular_file( i->status() ) ) continue;
    
      boost::smatch what;
      if( !boost::regex_match( i->path().filename().string(), what, my_filter ) ) continue;
    
      // File matches, store it
      all_matching_files.push_back( i->path().filename().string() );
    }     

    // Send out jobs to other processes 
    for (auto& file : all_matching_files) {
      // Receive a job request from another process
      int rank; // rank of the requesting process
      MPI_Status status;
      MPI_Recv(&rank, 1, MPI_INT, MPI_ANY_SOURCE, request_message_tag, MPI_COMM_WORLD, &status); 

      // Send data to the process that has asked for it  
      int i = 1;
      MPI_Send(&i, 1, MPI_INT, rank, num_message_tag, MPI_COMM_WORLD);
      MPI_Send_string(rank, data_message_tag, MPI_COMM_WORLD, file);
    }

    // Send a message to all other processes indicating that all calculations are done
    for (int proc=1; proc<world_size; ++proc) {
      int done_indicator = -1;  
      MPI_Send(&done_indicator, 1, MPI_INT, proc, num_message_tag, MPI_COMM_WORLD);
    }

  } else {
    // Setup a default network without output
    RunSetup netRunner; 
    netRunner.opts.DisableStdoutOutput = true;
    int number = 0;
    do {
      // Ask for some data from the main process, use a non-blocking send since we don't want 
      // to wait for the main process to receive it if we are done with the calculations 
      MPI_Request my_request;  // Never used because we are not worried about wether or 
                               // not rank 0 received our message as long as we get data
      MPI_Isend(&world_rank, 1, MPI_INT, 0, request_message_tag, MPI_COMM_WORLD, &my_request); 

      // Check if there are more calculations to be done
      MPI_Status status;
      MPI_Recv(&number, 1, MPI_INT, 0, num_message_tag, MPI_COMM_WORLD, &status); 
      if (number <= 0) break; // Leave the loop if there is nothing left 
       
      // Get the name of the particle data to run  
      std::string basename = MPI_Recv_string(0, data_message_tag, MPI_COMM_WORLD);
      std::cout << "My proc: " << world_rank << " Particle: " << basename <<  std::endl;
      
      // Run the particle if it has not been succesfully run before  
      netRunner.runName = basename;
      if (!boost::filesystem::is_regular_file(finishedDir + basename + ".h5")) {
        try { 
          netRunner.RunFromFile(baseDir + "/" + basename); 
          // If we got here, the run succeeded so move data to the finished directory
          boost::filesystem::rename(basename + ".log", finishedDir + basename + ".log");
          boost::filesystem::rename(basename + ".h5", finishedDir + basename + ".h5");
        } catch(std::exception& e) { 
          std::cerr << "  Particle " << basename << " failed with std::exception : " << e.what() << std::endl;  
        } catch(...) {
          std::cerr << "  Particle " << basename << " failed for some reason." << std::endl;  
        }
      } else { 
          std::cout << "  Particle " << basename << " has already been run." << std::endl;  

      }

    } while (number > 0);
  
  }  
  
  MPI_Finalize(); 
  return 0; 
}



