/// MPI wrapper testing 
/// 
///


#include <string>
#include <vector> 
#include <algorithm> 
#include <map> 
#include <iostream>

#include "RunSetup.hpp" 

int main(int /*nargin*/, char** /*args*/) {
  
  NetworkOptions opts; 
  opts.DisableStdoutOutput = false;
  opts.NSEEvolutionMinT9 = 7.0; 
  opts.EnableScreening = false; 
  RunSetup net("default", opts); 

  double ye = 0.01; 
  double entropy = 1.0;
  double tau = 3.e-2; 
  double tEnd = 1.e2;
  
  double T9Start = 8.0;
  net.runName = "run_Ye" + std::to_string(ye);
  net.RunTauEvolution(ye, entropy, tau, T9Start, tEnd); 
  //net.RunFromFile("/mnt/home/robe1024/test_density_trajectories/particle_102267_trajectory_00.txt"); 
  return 0; 
}



