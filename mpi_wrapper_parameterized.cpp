/// MPI wrapper testing 
/// 
///


#include <string>
#include <vector> 
#include <algorithm> 
#include <map> 
#include <iostream>
#include <mpi.h> 

#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

#include "mpi_helpers.hpp"
#include "RunSetup.hpp" 


int main(int nargin, char** args) {
  
  // Perform standard MPI initialization 
  int world_rank, world_size;
  MPI_Init(NULL, NULL); 
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank); 
  MPI_Comm_size(MPI_COMM_WORLD, &world_size); 
   
  // Message tags  
  const int request_message_tag = 1;
  const int num_message_tag = 2;
  const int data_message_tag = 4;
  const int ye_message_tag = 6; 
  const int s_message_tag = 7; 
  const int tau_message_tag = 8; 
   
  std::string baseDir =  "./";
  if (nargin>1) baseDir = std::string(args[1]); 
  
  std::string finishedDir =  "./";
  if (nargin>2) finishedDir = std::string(args[2]); 
  
  if (world_rank == 0) { 

    // Create arrays of Ye, s, and tau (in ms) values
    // Over write this to create the grid you want or change the loop below  
    std::vector<double> Yearr(20, 0.01), sarr(20, 1.0), tauarr(20, 5);  
    for (uint i = 1; i<Yearr.size(); ++i) Yearr[i] = Yearr[i-1] + 0.01; 

    // Send out jobs to other processes 
    for (uint i = 0; i<Yearr.size(); ++i) { 
      double Ye = Yearr[i];
      double s = sarr[i]; 
      double tau = tauarr[i]; 

      // Receive a job request from another process
      int rank; // rank of the requesting process
      MPI_Status status;
      MPI_Recv(&rank, 1, MPI_INT, MPI_ANY_SOURCE, request_message_tag, MPI_COMM_WORLD, &status); 

      // Send data to the process that has asked for it  
      int flag = 1;
      char buf[80];
      sprintf(buf, "run_Ye%4.2f_s%f_tau%f", Ye, s, tau);
      std::string runName(buf);
      MPI_Send(&flag, 1, MPI_INT, rank, num_message_tag, MPI_COMM_WORLD);
      MPI_Send(&Ye, 1, MPI_DOUBLE, rank, ye_message_tag, MPI_COMM_WORLD);
      MPI_Send(&s, 1, MPI_DOUBLE, rank, s_message_tag, MPI_COMM_WORLD);
      MPI_Send(&tau, 1, MPI_DOUBLE, rank, tau_message_tag, MPI_COMM_WORLD);
      MPI_Send_string(rank, data_message_tag, MPI_COMM_WORLD, runName);
    }

    // Send a message to all other processes indicating that all calculations are done
    for (int proc=1; proc<world_size; ++proc) {
      int done_indicator = -1;  
      MPI_Send(&done_indicator, 1, MPI_INT, proc, num_message_tag, MPI_COMM_WORLD);
    }

  } else {
    // Setup a default network without output
    NetworkOptions opts; 
    opts.DisableStdoutOutput = true;
    opts.NSEEvolutionMinT9 = 7.0; 
    opts.EnableScreening = false; 
    double T9Start = 8.0;
    
    RunSetup netRunner("default", opts); 
    int number = 0;
    do {
      // Ask for some data from the main process, use a non-blocking send since we don't want 
      // to wait for the main process to receive it if we are done with the calculations 
      MPI_Request my_request;  // Never used because we are not worried about wether or 
                               // not rank 0 received our message as long as we get data
      MPI_Isend(&world_rank, 1, MPI_INT, 0, request_message_tag, MPI_COMM_WORLD, &my_request); 

      // Check if there are more calculations to be done
      MPI_Status status;
      MPI_Recv(&number, 1, MPI_INT, 0, num_message_tag, MPI_COMM_WORLD, &status); 
      if (number <= 0) break; // Leave the loop if there is nothing left 
       
      // Get the parameters for the run 
      double Ye, s, tau; 
      MPI_Recv(&Ye, 1, MPI_DOUBLE, 0, ye_message_tag, MPI_COMM_WORLD, &status);  
      MPI_Recv(&s, 1, MPI_DOUBLE, 0, s_message_tag, MPI_COMM_WORLD, &status);  
      MPI_Recv(&tau, 1, MPI_DOUBLE, 0, tau_message_tag, MPI_COMM_WORLD, &status);  
      std::string basename = MPI_Recv_string(0, data_message_tag, MPI_COMM_WORLD);
      std::cout << "My proc: " << world_rank << " Particle: " << basename <<  std::endl;
      
      // Run the particle if it has not been succesfully run before  
      netRunner.runName = basename;
      if (!boost::filesystem::is_regular_file(finishedDir + basename + ".h5")) {
        try { 
          netRunner.RunTauEvolution(Ye, s, tau, T9Start);  
          // If we got here, the run succeeded so move data to the finished directory
          boost::filesystem::rename(basename + ".log", finishedDir + "/" + basename + ".log");
          boost::filesystem::rename(basename + ".h5", finishedDir + + "/" + basename + ".h5");
        } catch(std::exception& e) { 
          std::cerr << "  Particle " << basename << " failed with std::exception : " << e.what() << std::endl;  
        } catch(...) {
          std::cerr << "  Particle " << basename << " failed for some reason." << std::endl;  
        }
      } else { 
          std::cout << "  Particle " << basename << " has already been run." << std::endl;  

      }

    } while (number > 0);
  
  }  
  
  MPI_Finalize(); 
  return 0; 
}



